<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Carenergy */

$this->title = 'Create Carenergy';
$this->params['breadcrumbs'][] = ['label' => 'Carenergies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carenergy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
