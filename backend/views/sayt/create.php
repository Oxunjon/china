<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Sayt */

$this->title = 'Create Sayt';
$this->params['breadcrumbs'][] = ['label' => 'Sayts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sayt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
