<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Energy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="energy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'img1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'img2')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'img3')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'img4')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sell')->textInput() ?>

    <?= $form->field($model, 'show')->textInput() ?>

    <?= $form->field($model, 'active_is')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
