<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
  
 <?php if(!Yii::$app->user->isGuest):?>  
<!-- Main navbar -->
 <?php require_once('tpl_navigation.php') ?>
 <!-- /main navbar -->
<?php endif;?>

<!-- Page container -->
 <div class="page-container">

    <!-- Page content -->
    <div class="page-content">
     <?php if(!Yii::$app->user->isGuest):?> 
     <?php require_once('tpl_sidebar.php') ?>
   <?php endif;?>
     <!-- Main content -->
     <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-default" >
          <div class="page-header-content">
            <div class="page-title">
            
              <h5><span class="text-semibold"><?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                  ]) ?>
                  <?= Alert::widget() ?></span>
          </h5>
            
              
          </div>
        </div>

        </div>
        <!-- /page header -->

        <!-- Content area -->
        <div class="content">
        <?= $content ?>
        
        <?php require_once('tpl_footer.php') ?>

        </div>
        <!-- /content area -->

     </div>
     <!-- /main content -->

   </div>
   <!-- /page content -->

 </div>
 <!-- /page container -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
