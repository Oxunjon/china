<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use backend\models\Page;
use kartik\select2\Select2;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\Service */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uz_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ru_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'icon_style')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uz_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ru_description')->textarea(['rows' => 6]) ?>


    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?php
        echo $form->field($model, 'page')->widget(Select2::classname(), [
            'data' =>ArrayHelper::map(Page::find()->all(), 'id','uz_name'),
            'language' => 'uz',
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);


    ?>

    <?= $form->field($model, 'order_s')->textInput() ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ],
    ]); ?>
    <?= $form->field($model, 'target')->checkbox() ?>
    <?= $form->field($model, 'active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
