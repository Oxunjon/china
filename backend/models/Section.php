<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property string $uz_section
 * @property string $ru_section
 * @property string $en_section
 * @property string $x_section
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uz_section', 'ru_section', 'en_section', 'x_section'], 'required'],
            [['uz_section', 'ru_section', 'en_section', 'x_section'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uz_section' => Yii::t('app', 'Uz Section'),
            'ru_section' => Yii::t('app', 'Ru Section'),
            'en_section' => Yii::t('app', 'En Section'),
            'x_section' => Yii::t('app', 'X Section'),
        ];
    }
}
