<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "energy".
 *
 * @property int $id
 * @property string $name_uz
 * @property string $name_ru
 * @property string $cost
 * @property string $description_uz
 * @property string $description_ru
 * @property string $img1
 * @property string $img2
 * @property string $img3
 * @property string $img4
 * @property int $sell
 * @property int $show
 * @property int $active_is
 */
class Energy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'energy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['img1', 'img2', 'img3', 'img4'], 'string'],
            [['sell', 'show', 'active_is'], 'integer'],
            [['name_uz', 'name_ru', 'cost'], 'string', 'max' => 255],
            [['description_uz', 'description_ru'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_uz' => 'Name Uz',
            'name_ru' => 'Name Ru',
            'cost' => 'Cost',
            'description_uz' => 'Description Uz',
            'description_ru' => 'Description Ru',
            'img1' => 'Img1',
            'img2' => 'Img2',
            'img3' => 'Img3',
            'img4' => 'Img4',
            'sell' => 'Sell',
            'show' => 'Show',
            'active_is' => 'Active Is',
        ];
    }
}
