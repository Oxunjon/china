<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "zayavka".
 *
 * @property int $id
 * @property int $energy_id
 * @property string $fio
 * @property string $telephone
 * @property string $email
 * @property int $region_id
 * @property int $district_id
 * @property string $address
 * @property string $old_energy
 * @property int $count_energy
 * @property int $status
 * @property string $text
 * @property string $date
 * @property int $user_id
 */
class Zayavka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zayavka';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['energy_id', 'fio', 'telephone', 'email', 'region_id', 'district_id', 'address', 'old_energy', 'count_energy', 'status', 'text', 'user_id'], 'required'],
            [['energy_id', 'region_id', 'district_id', 'count_energy', 'status', 'user_id'], 'integer'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['fio', 'telephone', 'email', 'address', 'old_energy'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'energy_id' => 'Energy ID',
            'fio' => 'Fio',
            'telephone' => 'Telephone',
            'email' => 'Email',
            'region_id' => 'Region ID',
            'district_id' => 'District ID',
            'address' => 'Address',
            'old_energy' => 'Old Energy',
            'count_energy' => 'Count Energy',
            'status' => 'Status',
            'text' => 'Text',
            'date' => 'Date',
            'user_id' => 'User ID',
        ];
    }
}
