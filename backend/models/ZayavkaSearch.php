<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Zayavka;

/**
 * ZayavkaSearch represents the model behind the search form of `backend\models\Zayavka`.
 */
class ZayavkaSearch extends Zayavka
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'energy_id', 'region_id', 'district_id', 'count_energy', 'status', 'user_id'], 'integer'],
            [['fio', 'telephone', 'email', 'address', 'old_energy', 'text', 'date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zayavka::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'energy_id' => $this->energy_id,
            'region_id' => $this->region_id,
            'district_id' => $this->district_id,
            'count_energy' => $this->count_energy,
            'status' => $this->status,
            'date' => $this->date,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'old_energy', $this->old_energy])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
