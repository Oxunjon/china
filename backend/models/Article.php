<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $chapter_id
 * @property string $uz_article
 * @property string $ru_article
 * @property string $en_article
 * @property string $x_article
 * @property string $uz_article_tex
 * @property string $ru_article_tex
 * @property string $en_article_tex
 * @property string $x_article_tex
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chapter_id', 'uz_article', 'ru_article', 'en_article', 'x_article', 'uz_article_tex', 'ru_article_tex', 'en_article_tex', 'x_article_tex'], 'required'],
            [['chapter_id'], 'integer'],
            [['uz_article_tex', 'ru_article_tex', 'en_article_tex', 'x_article_tex'], 'string'],
            [['uz_article', 'ru_article', 'en_article', 'x_article'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'chapter_id' => Yii::t('app', 'Chapter ID'),
            'uz_article' => Yii::t('app', 'Uz Article'),
            'ru_article' => Yii::t('app', 'Ru Article'),
            'en_article' => Yii::t('app', 'En Article'),
            'x_article' => Yii::t('app', 'X Article'),
            'uz_article_tex' => Yii::t('app', 'Uz Article Tex'),
            'ru_article_tex' => Yii::t('app', 'Ru Article Tex'),
            'en_article_tex' => Yii::t('app', 'En Article Tex'),
            'x_article_tex' => Yii::t('app', 'X Article Tex'),
        ];
    }
}
