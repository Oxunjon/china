<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\web\BadRequestHttpException;
use frontend\components\BaseController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use backend\models\News;
use backend\models\Service;
use backend\models\Energy;
use backend\models\Carenergy;
use backend\models\Car;
use backend\models\Sayt;
use backend\models\Links;
use backend\models\Section;


/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @inheritdoc
     */

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $slide_news = News::find()->andWhere(['slider'=>[1],'active'=>[1]])->orderBy('id DESC')->limit(8)->all();
        $services = Service::find()->andWhere(['active'=>[1]])->orderBy('order_s ASC')->limit(3)->all();
        $energy = Energy::find()->andWhere(['active_is'=>[1]])->orderBy('id DESC')->limit(12)->all();
        $topenergy = Energy::find()->andWhere(['active_is'=>[1]])->orderBy('show DESC')->limit(15)->all();
        $links = Links::find()->andWhere(['active'=>[1]])->orderBy('id DESC')->all();
        $sayts = Sayt::find()->orderBy('id DESC')->all();
        $cars = Car::find()->orderBy('id DESC')->all();

        return $this->render('index',[
             'slide_news' => $slide_news,
             'services' => $services,
             'energy' => $energy,
             'topenergy' => $topenergy,
             'links'=>$links,
             'sayts' => $sayts,
             'cars' => $cars,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionSearch($q = NULL)
    {

       // echo $q;
        // $this->layout = 'columnAgency';
        if($q){
            $model = new \frontend\models\Search();
            $model->q = $q;
            $result = $model->search();
        }else{
            $result = [];
        }

        $services = Service::find()->andWhere(['active'=>[1],'user_id'=>[16,36,1]])->andWhere(['like', 'user_ids', 'adminxitoy'])->orderBy('order_s ASC')->all();


        return $this->render('search', [
             'services' => $services,
            'result' => $result,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $sections = Section::find()->all();
        $services = Service::find()->andWhere(['active'=>[1],'user_id'=>[16,36,1]])->andWhere(['like', 'user_ids', 'adminxitoy'])->orderBy('order_s ASC')->all();


        return $this->render('about',[
             'services' => $services,
            'sections'=>$sections,
        ]);
    }

    public function actionFilter($car_id)
    {
        $query = Carenergy::find()->Where(['car_id'=>$car_id])->with('energy')->with('car');
        //$query = Article::find()->where(['status' => 1]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $services = Service::find()->andWhere(['active'=>[1]])->orderBy('order_s ASC')->limit(3)->all();
        $mashina = Car::find()->orderBy('id Desc')->all();
        $links = Links::find()->andWhere(['active'=>[1]])->orderBy('id DESC')->all();
        $cars = array();
        return $this->render('filter',[
            // 'cars' => $cars,
             'pages' => $pages,
             'models' => $models,
             'services'=>$services,
             'links' => $links,
             'mashina' => $mashina,
             'cars' => $cars,
        ]);
    }

    public function actionList()
    {

        $selection=(array)Yii::$app->request->get('selection');
        $services = Service::find()->andWhere(['active'=>[1]])->orderBy('order_s ASC')->limit(3)->all();
        $mashina = Car::find()->orderBy('id Desc')->all();
        $links = Links::find()->andWhere(['active'=>[1]])->orderBy('id DESC')->all();
        $cars = Car::find()->where(['id' => $selection])->all();

        $query = Carenergy::find()->where(['car_id'=>$selection])
        ->with('energy')
        ->with('car');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('filter',[
          'pages' => $pages,
          'models' => $models,
          'services'=>$services,
          'links' => $links,
          'mashina' => $mashina,
          'cars' => $cars,
        ]);
    }

    public function actionMaps()
    {
        $sections = Section::find()->all();

        return $this->render('maps',[
            'sections'=>$sections,
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    // public function actionSignup()
    // {
    //     $model = new SignupForm();
    //     if ($model->load(Yii::$app->request->post())) {
    //         if ($user = $model->signup()) {
    //             if (Yii::$app->getUser()->login($user)) {
    //                 return $this->goHome();
    //             }
    //         }
    //     }

    //     return $this->render('signup', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
