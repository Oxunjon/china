<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Zayavka */
/* @var $form yii\widgets\ActiveForm */
$lang = Yii::$app->language;
?>
<div class="main-content shop-page main-content-detail">
  <div class="container">
    <br>
    <div class="row">
      <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 ">
        <div class="about-product row">
          <div class="zayavka-form">
            <div class="checkout-form content-form">

              <?php $form = ActiveForm::begin(); ?>

              <div class="row">
                <div class="col-md-4 col-sm-6">
                    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4 col-sm-6">
                    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4 col-sm-6">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'region_id')->textInput() ?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'district_id')->textInput() ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
              </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'old_energy')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'count_energy')->textInput() ?>
                </div>
              </div>

              <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

              <div class="form-group">
                  <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
              </div>

              <?php ActiveForm::end(); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 sidebar">
        <div class="equal-container widget-featrue-box">
          <div class="row">
            <?php foreach ($services as $key => $service):?>
            <div class="col-ts-12 col-xs-4 col-sm-12 col-md-12 col-lg-12 featrue-item">
              <div class="featrue-box layout2 equal-elem">
                <div class="block-icon">
                  <a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?>>
                    <?php if($service->icon_style):?><span class="<?= $service->icon_style;?>"><?php endif;?></span>
                  </a>
                </div>
                <div class="block-inner">
                  <a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?> class="title">
                    <?= $service->{$lang.'_name'};?>
                  </a>
                  <p class="des"><?= $service->{$lang.'_description'};?></p>
                </div>
              </div>
            </div>
          <?php endforeach;?>
          </div>
        </div>
        <div class="widget widget-banner row-banner">
          <div class="banner banner-effect1">
            <a href="#"><img src="images/banner23.jpg" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
