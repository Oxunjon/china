<?php $lang = Yii::$app->language;?>
<?php use yii\helpers\Url;?>
<div class="main-content home-page main-content-home2">
  <div class="container">
    <div class="main-slideshow slideshow2">
      <div class="owl-carousel nav-style1 owl-background" data-autoplay="true" data-nav="true" data-dots="false" data-loop="true" data-slidespeed="800" data-margin="0"  data-responsive = '{"0":{"items":1}, "640":{"items":1}, "768":{"items":1}, "1024":{"items":1}, "1200":{"items":1}}' data-height="400">
        <?php $i = 0;?>
        <?php foreach($slide_news as $key => $item):?>
        <?php $i++;?>
        <div class="slide-item item<?php echo $i;?> item-background" data-background="uploads/<?= $item->img;?>">
          <div class="slide-img" style="overflow:hidden; width:1170px; height:511px;">
            <img src="uploads/<?= $item->img;?>" alt="">
          </div>
          <div class="slide-content">
            <h4 class="subtitle" style="color:white;"><?= $item->{$lang.'_thema'}?></h5>
            <h6 class="title" style="color:white;"><?= $item->{$lang.'_description'}?></h3>
            <a href="<?= Url::to(['/news/view', 'id' => $item->id]) ?>" class="button"><?= Yii::t('app', 'Batafsil') ?></a>
          </div>
        </div>
      <?php endforeach;?>
      </div>
    </div>
    <div class="featrue-box-list equal-container">
      <div class="row">
        <?php foreach ($services as $key => $service):?>
        <div class="col-ss-12 col-xs-12 col-sm-4 col-md-4 col-lg-4 item">
          <div class="featrue-box layout1 equal-elem">
            <div class="block-icon">
              <a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?>>
                <?php if($service->icon_style):?><span class="<?= $service->icon_style;?>"><?php endif;?></span>
              </a>
            </div>
            <div class="block-inner">
              <a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?> class="title">
                <?= $service->{$lang.'_name'};?>
              </a>
              <p class="des"><?= $service->{$lang.'_description'};?></p>
            </div>
          </div>
        </div>
      <?php endforeach;?>
      </div>
    </div>


  </div>
  <div class="products-view">
    <div class="container">
      <div class="section-head box-has-content">
        <h4 class="section-title"><?= Yii::t('app', 'Ohirgi qo\'shilganlar') ?></h4>
      </div>
      <div class="section-content">
        <div class="owl-carousel product-list-owl nav-style2 equal-container" data-autoplay="false" data-nav="true" data-dots="false" data-loop="false" data-slidespeed="800" data-margin="0"  data-responsive = '{"0":{"items":1}, "480":{"items":2,"margin":0}, "700":{"items":3,"margin":-1}, "992":{"items":4}, "1200":{"items":4}}'>
          <?php foreach ($energy as $key => $value):?>
            <div class="product-item layout1">
            <div class="product-inner equal-elem">
              <div class="thumb" style="overflow:hidden; width:271px; height:271px;">
                <a href="<?= Url::to(['/energy/view', 'id' => $value->id  ])?>" class="quickview-button"><span class="icon"><i class="fa fa-eye" aria-hidden="true"></i></span> <?= Yii::t('app', 'Batafsil') ?></a>
                <a href="<?= Url::to(['/energy/view', 'id' => $value->id  ])?>" class="thumb-link"><img src="uploads/<?= $value->img1;?>" alt=""></a>
              </div>
              <div class="info">
                <a href="detail.html" class="product-name"><?= $value->{'name_'.$lang};?></a>
                <div class="price">
                  <span <?php if($value->sell):?>class="del"<?php endif;?>><?= $value->cost;?> <?= Yii::t('app', 'so\'m') ?></span>
                  <?php if($value->sell):?><span class="ins"><?= $value->sell;?> <?= Yii::t('app', 'so\'m') ?></span><?php endif;?>
                </div>
              </div>
              <div class="group-button">
                <div class="inner">
                  <a href="#" class="add-to-cart"><span class="text"><?= Yii::t('app', 'Buyurtma berish') ?></span><span class="icon"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></span></a>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach;?>

          </div>
        </div>
    </div>
  </div>

  <div class="products-view">
    <div class="container">
      <div class="section-head box-has-content">
        <h4 class="section-title"><?= Yii::t('app', 'Eng ko\'p ko\'rilganlar') ?></h4>
      </div>
      <div class="section-content">
        <div class="owl-carousel product-list-owl nav-style2 equal-container" data-autoplay="false" data-nav="true" data-dots="false" data-loop="false" data-slidespeed="800" data-margin="0"  data-responsive = '{"0":{"items":1}, "480":{"items":2,"margin":0}, "700":{"items":3,"margin":-1}, "992":{"items":4}, "1200":{"items":4}}'>
          <?php foreach ($topenergy as $key => $topvalue):?>
            <div class="product-item layout1">
            <div class="product-inner equal-elem">
              <div class="thumb" style="overflow:hidden; width:271px; height:271px;">
                <a href="<?= Url::to(['/energy/view', 'id' => $topvalue->id  ])?>" class="quickview-button"><span class="icon"><i class="fa fa-eye" aria-hidden="true"></i></span> <?= Yii::t('app', 'Batafsil') ?></a>
                <a href="<?= Url::to(['/energy/view', 'id' => $topvalue->id  ])?>" class="thumb-link"><img src="uploads/<?= $topvalue->img1;?>" alt=""></a>
              </div>
              <div class="info">
                <a href="detail.html" class="product-name"><?= $topvalue->{'name_'.$lang};?></a>
                <div class="price">
                  <span <?php if($topvalue->sell):?>class="del"<?php endif;?>><?= $topvalue->cost;?> <?= Yii::t('app', 'so\'m') ?></span>
                  <?php if($topvalue->sell):?><span class="ins"><?= $topvalue->sell;?> <?= Yii::t('app', 'so\'m') ?></span><?php endif;?>
                </div>
              </div>
              <div class="group-button">
                <div class="inner">
                  <a href="#" class="add-to-cart"><span class="text"><?= Yii::t('app', 'Buyurtma berish') ?></span><span class="icon"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></span></a>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach;?>

          </div>
        </div>
    </div>
  </div>

  <div class="brand">
    <div class="container">
      <div class="owl-carousel" data-autoplay="false" data-nav="false" data-dots="false" data-loop="false" data-slidespeed="800" data-margin="30"  data-responsive = '{"0":{"items":2}, "480":{"items":2}, "640":{"items":3}, "1024":{"items":4}, "1200":{"items":5}}'>
        <?php foreach($links as $key => $link):?>
        <div class="brand-item"><a href="<?= $link->link;?>" target="_blank"><img src="uploads/<?= $link->uz_img?>" alt="" title="<?= $link{$lang.'_name'}?>"></a></div>
        <?php endforeach;?>
      </div>
    </div>
  </div>
</div>
  <!-- End Page -->
