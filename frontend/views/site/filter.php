<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use backend\models\Menu;
use backend\models\SubMenu;
use backend\models\Symbol;
use backend\models\TopMenu;
use backend\models\Chapter;
use backend\models\Article;
use yii\widgets\LinkPager;


use yii\helpers\Url;
$lang = Yii::$app->language;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="main-content shop-page main-content-grid-right">
  <div class="container">
    <div class="breadcrumbs">
      <a href="#"></a>  <span class="current"></span>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 content-offset">
        <div class="main-banner">
          <div class="banner banner-effect1">
            <a href="#"><img src="images/banner22.jpg" alt=""></a>
          </div>
        </div>
        <div class="categories-content">
          <?php if(empty($cars)):?>
          <h4 class="shop-title"><?php echo $models['0']['car']['name_'.$lang];?></h4>
        <?php else:?>
          <h4 class="shop-title"><?php foreach($cars as $key => $car):?>&nbsp;/&nbsp;<?= $car{'name_'.$lang};?>&nbsp;/&nbsp;<?php endforeach;?></h4>
        <?php endif;?>
          <div class="top-control box-has-content">
            <div class="control">
              <div class="control-button">
                <a href="#" class="grid-button active"><span class="icon"><i class="fa fa-th-large" aria-hidden="true"></i> </span>Grid</a>
                <a href="#" class="list-button"><span class="icon"><i class="fa fa-th-list" aria-hidden="true"></i></span> List</a>
              </div>
            </div>
          </div>
          <div class="product-container auto-clear grid-style equal-container box-has-content">
            <?php foreach($models as $key => $value):?>
            <div class="product-item layout1 col-ts-12 col-xs-6 col-sm-6 col-md-4 col-lg-4 no-padding">
              <div class="product-inner equal-elem">
                <div class="thumb" style="overflow:hidden; width:271px; height:271px;">
                  <a href="<?= Url::to(['/energy/view', 'id' => $value->energy->id  ])?>" class="quickview-button"><span class="icon"><i class="fa fa-eye" aria-hidden="true"></i></span> <?= Yii::t('app', 'Batafsil') ?></a>
                  <a href="<?= Url::to(['/energy/view', 'id' => $value->energy->id  ])?>" class="thumb-link"><img src="uploads/<?= $value->energy->img1;?>" alt=""></a>
                </div>
                <div class="info">
                  <a href="detail.html" class="product-name"><?= $value->energy->{'name_'.$lang};?></a>
                  <p class="description"><?= $value->energy->{'description_'.$lang};?></p>
                  <div class="price">
                    <span <?php if($value->energy->sell):?>class="del"<?php endif;?>><?= $value->energy->cost;?> <?= Yii::t('app', 'so\'m') ?></span>
                    <?php if($value->energy->sell):?><span class="ins"><?= $value->energy->sell;?> <?= Yii::t('app', 'so\'m') ?></span><?php endif;?>
                  </div>
                </div>
                <div class="group-button">
                  <div class="inner">
                    <a href="#" class="add-to-cart"><span class="text"><?= Yii::t('app', 'Buyurtma berish') ?></span><span class="icon"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></span></a>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach;?>
          </div>
          <div class="pagination">
            <?php
                echo LinkPager::widget([
                    'pagination' => $pages,
                ]);
            ?>

          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 sidebar">
        <div class="widget widget-categories">
          <h5 class="widgettitle"><?= Yii::t('app', 'Avtomobil turi bo\'yicha') ?></h5>
          <?=Html::beginForm(['site/list'],'get');?>
          <ul class="list-categories">
            <?php foreach ($mashina as $key => $mashin):?>
            <input type="checkbox" id="<?= $mashin->id;?>" name="selection[]" value="<?= $mashin->id;?>"><label for="<?= $mashin->id;?>" class="label-text"><?= $mashin{'name_'.$lang}?></label><br>
          <?php endforeach;?>

          </ul>
          <?= Html::submitButton('Filter', ['class' => 'btn btn-info']);?>
          <?= Html::endForm();?>
        </div>
        <div class="widget widget-banner row-banner">
          <div class="banner">
            <a href="#"><img src="images/banner21.jpg" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="brand">
    <div class="container">
      <div class="owl-carousel" data-autoplay="false" data-nav="false" data-dots="false" data-loop="false" data-slidespeed="800" data-margin="30"  data-responsive = '{"0":{"items":2}, "640":{"items":3}, "768":{"items":3}, "1024":{"items":4}, "1200":{"items":5}}'>
        <?php foreach($links as $key => $link):?>
        <div class="brand-item"><a href="<?= $link->link;?>" target="_blank"><img src="uploads/<?= $link->uz_img?>" alt="" title="<?= $link{$lang.'_name'}?>"></a></div>
        <?php endforeach;?><div class="brand-item"><a href="#"><img src="images/brand1.jpg" alt=""></a></div>

      </div>
    </div>
  </div>
</div>
