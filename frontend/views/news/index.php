<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use backend\models\Menu;
use backend\models\Symbol;
use backend\models\SubMenu;
use backend\models\TopMenu;

$menus = Menu::find()->andWhere(['active'=>[1]])->all();
$topmenus = TopMenu::find()->all();
$symbol= Symbol::find()->all();

$lang = Yii::$app->language;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page -->
  <div class="page animsition" style="top: 40px;">
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-md-9">
          <!-- Panel -->
          <div class="panel">
            <div class="panel-body nav-tabs-animate nav-tabs-horizontal">
              <div class="tab-content">
                <div class="tab-pane active animation-slide-left" id="activities" role="tabpanel">
                  <ul class="list-group list-group-dividered list-group-full">
            <?php foreach ($models as $key => $e_news):?>
                    <?php if ($e_news->{$lang.'_thema'}): ?>
                <li class="list-group-item">
                  <div class="media">
                    <div class="media-left">
                      <?php if($e_news->img!=NULL):?>  
                      <a class="avatar avatar-online hidden-xs" href="<?= Url::to(['/news/view', 'id' => $e_news->id]) ?>" style="width: 250px; height: 150px; border-radius: 0px;">
                        <img src="<?= $e_news->img;?>" alt="" style="border-radius: 0px;"><i></i>
                      </a>
                      <?php endif;?>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">
                        <a class="name" href="<?= Url::to(['/news/view', 'id' => $e_news->id]) ?>"><?= $e_news->{$lang.'_thema'}?></a>
                      </h4>
                      <small><i class="icon fa-calendar" aria-hidden="true"></i><?=$e_news->date;?></small>&nbsp;
                      <small><i class="icon fa-eye" aria-hidden="true"></i><?=$e_news->show_n;?></small>
                      <p style="text-align: justify;"><?= $e_news->{$lang.'_description'}?></p>
                    </div>
                  </div>
                </li>
                <?php endif;?>
            <?php endforeach;?>   
              </ul>
                </div>
              </div>
            </div>
          </div>
          <?php
                                        echo LinkPager::widget([
                                            'pagination' => $pages,
                                        ]);
                                    ?>
          <!-- End Panel -->
        </div>
        <div class="col-md-3">
          <!-- Page Widget -->
          <div class="widget widget-shadow text-center">
            <div class="widget-header">
              <div class="widget-header-content">
                    <?php foreach ($services as $key => $service):?>
                        <div class="col-lg-12 col-md-12" >
                          <div class="widget widget-shadow text-center">
                            <div class="widget-header cover overlay" style="height: calc(100% - 100px);">
                              <img class="cover-image" src="/images/ppp.jpg" alt="..." style="height: 100%;">
                              <div class="overlay-panel vertical-align">
                                <div class="vertical-align-middle">
                                  <a class="avatar avatar-100 bg-white margin-bottom-10 margin-xs-0 img-bordered" <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?> style="width: 214px;border-radius: 0px;">
                                    <img src="/uploads/<?= $service->icon;?>" alt="" style="border-radius: 0px;">
                                  </a>
                                  <div class="font-size-15" style="color: #0074b4"><?= $service->{$lang.'_name'};?></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <?php endforeach;?>
                
              </div>
            </div>
          </div>
          <!-- End Page Widget -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->

