<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Energy */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Energies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php $lang = Yii::$app->language;?>
<div class="energy-view">



    <div class="main-content shop-page main-content-detail">
  		<div class="container">
  			<div class="breadcrumbs">
  				<a href="<?= Url::to(['/']) ?>">jazd.uz</a> \ <span class="current"><?= Yii::t('app', 'detail') ?></span>
  			</div>
  			<div class="row">
  				<div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 content-offset">
  					<div class="about-product row">
  						<div class="details-thumb col-xs-12 col-sm-12 col-md-6 col-lg-6">
  							<div class="owl-carousel nav-style3 has-thumbs" data-autoplay="false" data-nav="true" data-dots="false" data-loop="true" data-slidespeed="800" data-margin="0"  data-responsive = '{"0":{"items":1}, "480":{"items":2}, "768":{"items":1}, "1024":{"items":1}, "1200":{"items":1}}'>
  								<div class="details-item"><img src="uploads/<?= $model->img1;?>" alt=""></div>
  								<div class="details-item"><img src="uploads/<?= $model->img2;?>" alt=""></div>
  								<div class="details-item"><img src="uploads/<?= $model->img3;?>" alt=""></div>
  								<div class="details-item"><img src="uploads/<?= $model->img4;?>" alt=""></div>
  							</div>
  						</div>
  						<div class="details-info col-xs-12 col-sm-12 col-md-6 col-lg-6">
  							<a href="detail.html" class="product-name"><?= $model{'name_'.$lang};?></a>
  							<hr>
  							<div class="price">
                  <?php if($model->sell):?><s><h6><?= $model->cost;?> <?= Yii::t('app', 'so\'m') ?></h6></s><?php else:?><?= $model->cost;?> <?= Yii::t('app', 'so\'m') ?><?php endif;?> </span>
                  <?php if($model->sell):?><span class="ins"><?= $model->sell;?> <?= Yii::t('app', 'so\'m') ?></span><?php endif;?>
  							</div>
                <hr>
  							<div class="group-social">
  								<ul class="list-socials">
  									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
  									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
  									<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
  									<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
  									<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
  								</ul>
  							</div>
  							<div class="quantity">

                              </div>
                              <div class="group-button">
  								<div class="inner">
  									<a href="#" class="add-to-cart"><span class="text"><?= Yii::t('app', 'Buyurtma berish') ?></span><span class="icon"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></span></a>

  								</div>
  							</div>
  						</div>
  					</div>
  					<div class="kt-tab nav-tab-style2">
  						<ul class="nav list-nav">
  							<li class="active"><a data-animated="fadeIn" data-toggle="pill" href="#tab1"><?= Yii::t('app', 'Batafsil') ?></a></li>
  						</ul>
  						<div class="tab-content">
  				    		<div id="tab1" class="tab-panel active ">
  				    			<div class="description">
  					    			<?= $model{'description_'.$lang};?>
  				    			</div>
  				    		</div>
  				    	</div>
  					</div>
  				</div>
  				<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 sidebar">
  					<div class="equal-container widget-featrue-box">
  						<div class="row">
                <?php foreach ($services as $key => $service):?>
  							<div class="col-ts-12 col-xs-4 col-sm-12 col-md-12 col-lg-12 featrue-item">
  								<div class="featrue-box layout2 equal-elem">
  									<div class="block-icon">
                      <a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?>>
                        <?php if($service->icon_style):?><span class="<?= $service->icon_style;?>"><?php endif;?></span>
                      </a>
                    </div>
  									<div class="block-inner">
  										<a <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?> class="title">
                        <?= $service->{$lang.'_name'};?>
                      </a>
  										<p class="des"><?= $service->{$lang.'_description'};?></p>
  									</div>
  								</div>
  							</div>
              <?php endforeach;?>
  						</div>
  					</div>
  					<div class="widget widget-banner row-banner">
  						<div class="banner banner-effect1">
  							<a href="#"><img src="images/banner23.jpg" alt=""></a>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="products-arrivals">
  				<div class="section-head box-has-content">
  					<h4 class="section-title"><?= Yii::t('app', 'Eng ko\'p ko\'rilganlar') ?></h4>
  				</div>
  				<div class="section-content">
  					<div class="owl-carousel product-list-owl nav-style2 equal-container" data-autoplay="false" data-nav="true" data-dots="false" data-loop="false" data-slidespeed="800" data-margin="0"  data-responsive = '{"0":{"items":1}, "480":{"items":2}, "650":{"items":3}, "1024":{"items":4}, "1200":{"items":5}}'>
              <?php foreach ($topenergy as $key => $topvalue):?>
                <div class="product-item layout1">
  							<div class="product-inner equal-elem">
  								<div class="thumb" style="overflow:hidden; width:214px; height:214px;">
  									<a href="<?= Url::to(['/energy/view', 'id' => $topvalue->id  ])?>" class="quickview-button">
                      <span class="icon"><i class="fa fa-eye" aria-hidden="true"></i></span> <?= Yii::t('app', 'Batafsil') ?>
                    </a>
  									<a href="<?= Url::to(['/energy/view', 'id' => $topvalue->id  ])?>" class="thumb-link"><img src="uploads/<?= $topvalue->img1;?>" alt=""></a>
  								</div>
  								<div class="info">
  									<a href="<?= Url::to(['/energy/view', 'id' => $topvalue->id  ])?>" class="product-name"><?= $topvalue->{'name_'.$lang};?></a>
  									<div class="price">
                      <span <?php if($topvalue->sell):?>class="del"<?php endif;?>><?= $topvalue->cost;?> <?= Yii::t('app', 'so\'m') ?></span>
                      <?php if($topvalue->sell):?><span class="ins"><?= $topvalue->sell;?> <?= Yii::t('app', 'so\'m') ?></span><?php endif;?>
  									</div>
  								</div>
  								<div class="group-button">
  									<div class="inner">
  										<a href="#" class="add-to-cart"><span class="text"><?= Yii::t('app', 'Buyurtma berish') ?></span><span class="icon"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></span></a>

  									</div>
  								</div>
  							</div>
  						</div>
            <?php endforeach;?>
  	    			</div>
  	    		</div>
  			</div>
  		</div>
      <div class="brand">
        <div class="container">
          <div class="owl-carousel" data-autoplay="false" data-nav="false" data-dots="false" data-loop="false" data-slidespeed="800" data-margin="30"  data-responsive = '{"0":{"items":2}, "480":{"items":2}, "640":{"items":3}, "1024":{"items":4}, "1200":{"items":5}}'>
            <?php foreach($links as $key => $link):?>
            <div class="brand-item"><a href="<?= $link->link;?>" target="_blank"><img src="uploads/<?= $link->uz_img?>" alt="" title="<?= $link{$lang.'_name'}?>"></a></div>
            <?php endforeach;?>
          </div>
        </div>
      </div>
  	</div>
